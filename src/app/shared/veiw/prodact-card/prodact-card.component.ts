import { Component, OnInit } from '@angular/core';
import { Product } from '../../model/product';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-prodact-card',
  templateUrl: './prodact-card.component.html',
  styleUrls: ['./prodact-card.component.scss']
})
export class ProdactCardComponent implements OnInit {

  constructor(private apiService: ApiService) { }
  productList: Product[] = []

  ngOnInit(): void {
    this.apiService.getProducts().subscribe((data: any[]) => {
      this.productList = data
      // console.log(this.productList);

      let y= this.productList.map(y => {
        y.star = y.star.toLocaleString('fa-ir'),
        y.price = y.price.toLocaleLowerCase('fa-ir')
        console.log(y);
        return y
      })
   
      
    })
  }

}
