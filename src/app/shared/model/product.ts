export interface Product {
    id:number
    name:string
    image: string
    price: string|string
    star:number|string
}