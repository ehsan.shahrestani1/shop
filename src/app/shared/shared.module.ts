import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProdactCardComponent } from './veiw/prodact-card/prodact-card.component';
import { NavBarComponent } from './veiw/nav-bar/nav-bar.component';
import { FooterComponent } from './veiw/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    ProdactCardComponent,
    NavBarComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports:[
    NavBarComponent,
    FooterComponent,
    ProdactCardComponent
  ]
})
export class SharedModule { }
